import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'
import Axios from 'axios'
import { createProvider } from './vue-apollo'
import VueToastr from '@deveodk/vue-toastr'
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'
import Vuelidate from 'vuelidate'

 
Vue.use(Vuelidate)

Vue.use(VueToastr, {
  defaultPosition: 'toast-top-right',
  defaultType: 'info',
  defaultTimeout: 7000
})

Vue.prototype.$http = Axios;
const token = localStorage.getItem('token')

Vue.config.productionTip = false


Vue.use(ArgonDashboard)
new Vue({
  router,
  apolloProvider: createProvider(),
  render: h => h(App)
}).$mount('#app')
